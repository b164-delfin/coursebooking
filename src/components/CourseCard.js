import React, {useState, useEffect} from 'react';
import {Card, Button,} from 'react-bootstrap';

export default function CourseCard({courseProp}) {

	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter, setter] = useState(initalGetterValue)
	*/
	const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)


	function enroll() {

			setCount(count + 1)
			setSeat(seat - 1)
		}

		useEffect(() => {
			if(seat === 0) {
				alert("No more seats available!");
			}
		}, [seat]);


	//console.log(props.courseProp)
	const {name, description, price} = courseProp

	return(

			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Text> <strong>Enrollees</strong></Card.Text>
					<Card.Text>{count} Enrollees</Card.Text>
					<Card.Text>{seat} Seats</Card.Text>
					<Button variant="primary" onClick={enroll}>Enroll</Button>
				</Card.Body>
			</Card>
		)

}
